'use strict';

const _ = require('lodash');
const request = require('request-promise-native');

const logger = require('winston');

const RetryQueue = require('./retry-queue');

class Zendesk {
    constructor(baseurl, user, config) {
        this.user = user;
        this.token = config.token;
        this.baseurl = baseurl;
        this.defaultOptions = {
            auth: {
                user: `${this.user}/token`,
                password: this.token
            },
            json: true
        };
        this.jobQueue = new RetryQueue();
    }
    request(overrides) {
        const parameters = _.defaults(overrides, this.defaultOptions);
        const uri = overrides.uri;
        const method = parameters.method || 'GET';
        logger.log('verbose', `zendesk: ${method} ${uri}`);

        return this.jobQueue.push(() => {
            return request(parameters)
                .then(res => RetryQueue.result(res))
                .catch(err => {
                    if (err.statusCode === 429) {
                        const retryAfter = err.response.headers['retry-after'] || 5;
                        logger.info('Hit request limit, ' + 
                            `sleeping for ${retryAfter} seconds before trying again`);
                        return Promise.resolve(RetryQueue.result(null, retryAfter * 1000));
                    }
                    return Promise.reject(err);
                });
        });
    }
    getArticles() {
        return this.request({
            uri: `${this.baseurl}/api/v2/help_center/articles.json`
        });
    }
    createArticle(section, article) {
        const locale = article.locale;

        return this.request({
            method: 'POST',
            uri: `${this.baseurl}/api/v2/help_center/${locale}/sections/${section}/articles.json`,
            body: {
                article: {
                    title: article.title,
                    body: article.body,
                    locale: article.locale
                }
            }
        });
    }
    updateArticle(id, article) {
        return this.request({
            method: 'PUT',
            uri: `${this.baseurl}/api/v2/help_center/articles/${id}.json`,
            body: {
                article: {
                    title: article.title,
                    body: article.body,
                    locale: article.locale
                }
            }
        });
    }
    getTranslations(articleId) {
        return this.request({
            uri: `${this.baseurl}/api/v2/help_center/articles/${articleId}/translations.json`
        });
    }
    getTranslation(articleId, locale) {
        return this.request({
            uri: `${this.baseurl}/api/v2/help_center/` +
            `articles/${articleId}/translations/${locale}.json`
        });
    }
    _updateTranslation(articleId, locale, data) {
        return this.request({
            method: 'PUT',
            uri: `${this.baseurl}/api/v2/help_center/` +
            `articles/${articleId}/translations/${locale}.json`,
            body: {
                translation: data
            }
        });
    }
    updateTranslationTitle(articleId, locale, title) {
        return this._updateTranslation(articleId, locale, { title: title });
    }
    updateTranslationBody(articleId, locale, body) {
        return this._updateTranslation(articleId, locale, { body: body });
    }
    createTranslation(articleId, locale, translation) {
        return this.request({
            method: 'POST',
            uri: `${this.baseurl}/api/v2/help_center/articles/${articleId}/translations.json`,
            body: {
                translation: {
                    title: translation.title,
                    body: translation.body,
                    locale: locale
                }
            }
        });
    }
    getSectionsForLocale(locale) {
        return this.request({
            uri: `${this.baseurl}/api/v2/help_center/${locale}/sections.json`
        });
    }
    createSection(locale, categoryId, name) {
        return this.request({
            method: 'POST',
            uri: `${this.baseurl}/api/v2/help_center/` +
            `${locale}/categories/${categoryId}/sections.json`,
            body: {
                section: {
                    name,
                    locale
                }
            }
        });
    }
}

module.exports = Zendesk;
