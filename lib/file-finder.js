'use strict';

const fs = require('fs');
const path = require('path');

class FileFinder {
    constructor(directory) {
        this.directory = directory;
    }
    _readAllFiles() {
        return new Promise((resolve, reject) => {
            fs.readdir(this.directory, (err, files) => {
                if (err) {
                    return reject(err);
                }
                resolve(files);
            });
        });
    }
    getMarkdownFiles() {
        return this._readAllFiles()
            .then(files => files
                .filter(f => !f.startsWith('.'))
                .filter(f => f.endsWith('.md'))
                .map(f => path.join(this.directory, f))
            );
    }
}

module.exports = FileFinder;
