'use strict';

const EventEmitter = require('events');

class Queue extends EventEmitter {
    constructor() {
        super();
        this.que = [];
        this.running = false;
    }
    push(fn) {
        return new Promise((resolve, reject) => {
            this.que.push({
                fn: fn,
                promise: {
                    resolve: resolve,
                    reject: reject
                }
            });
            this._start();
        });
    }
    _start() {
        if (this.running) {
            return;
        }
        this.running = true;
        this._doNextTask();
    }
    _doNextTask() {
        process.nextTick(() => this._run());
    }
    _run() {
        const task = this.que.shift();
        if (!task) {
            this.running = false;
            this.emit('completed');
            return;
        }

        task.fn().then(res => {
            if (res.retry) {
                this.que.unshift(task);
                setTimeout(() => this._doNextTask(), res.retry);
            }
            else {
                task.promise.resolve(res.result);
                this._doNextTask();
            }
        }).catch(err => {
            task.promise.reject(err);
            this._doNextTask();
        });
    }
}

Queue.result = function(result, retry) {
    return {
        result: result,
        retry: typeof retry === 'number' ? retry : 0
    };
};

module.exports = Queue;
