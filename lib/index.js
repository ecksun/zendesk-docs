'use strict';

const _ = require('lodash');
const logger = require('winston');

const DocumentFinder = require('./document-finder');
const Zendesk = require('./zendesk');
const Publisher = require('./article').Publisher;
const Sections = require('./sections');

logger.level = 'silly';

if (!process.argv[2]) {
    console.error('Give the directory of the documents as the first argument');
    process.exit(1);
}


const zendesk = new Zendesk('https://manetos.zendesk.com', process.env.ZENDESK_USER, {
    token: process.env.ZENDESK_API_TOKEN,
});

const sections = new Sections(zendesk);

const documentFinder = new DocumentFinder(process.argv[2] );
const documentsPromise = documentFinder.findDocuments();

const sectionsCreatedPromise = documentsPromise
    .then(articles => {
        const sourcePromises = articles.map(
            article => sections.ensureSectionsExist(
                article.source.data.locale, article.source.data.section)
        );
        const translationPromises = _.flatMap(articles,
            article => article.translations.map(
                translation => sections.ensureSectionsExist(
                    translation.data.locale, translation.data.section)
            ));

        return Promise.all([...sourcePromises, ...translationPromises]);
    })
    .catch(err => console.error(err));

Promise.all([documentsPromise, sectionsCreatedPromise])
    .then(res => res[0])
    .then(articles => articles.map(article => new Publisher(article, zendesk, sections)))
    .then(publishers => publishers.forEach(publisher => publisher.updateOrPublish()
        .then(() => {
            const translationLocales = publisher.article.translations.map(t => t.data.locale);
            const sourceLocale = publisher.article.source.data.locale;
            const locales = [...translationLocales, sourceLocale];
            console.info(`Published ${publisher.article.source.data.title} ` +
                `for locales: ${locales.join(', ')}`);
        })
        .catch(err => logger.error(
            `Failed to publish ${publisher.article.file.file} because of ${err}`))
    ))
    .catch(err => console.error(err));
